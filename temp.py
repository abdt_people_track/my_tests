import cv2
from time import time
from os.path import isdir
from os import mkdir


def timer(func):
    def inner(*args, **kwargs):
        start_time = time()
        func(*args, **kwargs)
        print("--- %s seconds ---" % (time() - start_time))

    return inner


@timer
def read_write_video(input_path, output_path):
    uri = "rtsp://178.207.8.252:6604/MTUsMywyMzcwMjgsMSwwLDAsMA=="
    latency = 10
    gst_str = ("rtspsrc location={} latency={} ! " "rtph264depay ! h264parse ! avdec_h264 ! " " autovideoconvert ! autovideosink ").format(uri, latency)

    cap = cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)
    fwidth, fheight = int(cap.get(3)), int(cap.get(4))
    print(fwidth, fheight)
    fourcc = cv2.VideoWriter_fourcc(*"XVID")
    out = cv2.VideoWriter(output_path, fourcc, 25.0, (fwidth, fheight))
    while cap.isOpened():
        check, frame = cap.read()
        if check:
            out.write(frame)
            cv2.imshow("kek", frame)
        else:
            break
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break
    cap.release()
    out.release()


# @timer
# def read_write_video(input_path, output_path):
#     cap = cv2.VideoCapture(input_path)
#     fwidth, fheight = int(cap.get(3)), int(cap.get(4))
#     fourcc = cv2.VideoWriter_fourcc(*"XVID")
#     out = cv2.VideoWriter(output_path, fourcc, 25.0, (fwidth, fheight))
#     while cap.isOpened():
#         check, frame = cap.read()
#         if check:
#             out.write(frame)
#             cv2.imshow("kek", frame)
#         else:
#             break
#         if cv2.waitKey(1) & 0xFF == ord("q"):
#             break
#     cap.release()
#     out.release()


if __name__ == "__main__":
    input_path = "./input_videos/50.avi"
    camera_1 = "rtsp://178.207.8.252:6604/MTUsMywyMzcwMjgsMCwwLDAsMA=="
    camera_2 = "rtsp://178.207.8.252:6604/MTUsMywyMzcwMjgsMSwwLDAsMA=="
    camera_3 = "rtsp://178.207.8.252:6604/MTUsMywyMzcwMjgsMywwLDAsMA=="

    output_path = "./output_videos/test.avi"
    if not isdir("./output_videos/"):
        mkdir("./output_videos/")
    read_write_video(camera_2, output_path)
